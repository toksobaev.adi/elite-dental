$(function () {
  setTimeout(function () {
    var preloader = document.getElementById("page-preloader");
    if (!preloader.classList.contains("done")) {
      preloader.classList.add("done");
    }
  }, 1500);

  const phone = document.getElementById("phone");
  if (phone) {
    IMask(phone, {
      mask: "+{1} (000)000-0000",
    });
  }

  Fancybox.bind('[data-fancybox="request-an-appointment"]', {
    groupAttr: false,
  });

  $("#virtual-tour-btn").click(function (event) {
    event.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $("#virtual-tour").offset().top - 120,
      },
      500
    );
  });

  $("#contacts").click(function (event) {
    event.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $(".contact-section").offset().top - 120,
      },
      500
    );
  });

  $(window).scroll(function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 148) {
      $(".header").addClass("nav");
    } else {
      $(".header").removeClass("nav");
    }
  });

  $(".main-carousel").slick({
    dots: false,
    infinite: true,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 5000,
    adaptiveHeight: true,
  });

  $(".office-carousel").slick({
    arrows: false,
    dots: true,
    infinite: true,
  });

  $(".btn_mnu").click(function () {
    $(this).toggleClass("active");
    $(".menu").toggleClass("open");
    $(".menu__bg").toggleClass("open");
  });

  $(".menu li a").click(function () {
    $(".menu").removeClass("open");
    $(".menu__bg").removeClass("open");
    $(".btn_mnu").removeClass("active");
  });

  $(".menu__bg").click(function () {
    $(".menu").removeClass("open");
    $(".menu__bg").removeClass("open");
    $(".btn_mnu").removeClass("active");
  });

  $(".request-an-appointment__form").submit(function () {
    $.ajax({
      type: "POST",
      url: "mail.php",
      data: $(this).serialize(),
    }).done(function () {
      $(this).find("input").val("");
      Fancybox.close();
      Fancybox.show([{ src: "#thanks-sign-up", type: "inline" }]);
      $(".request-an-appointment__form").trigger("reset");
    });
    return false;
  });

  $(".appointment").submit(function () {
    $.ajax({
      type: "POST",
      url: "mail2.php",
      data: $(this).serialize(),
    }).done(function () {
      $(this).find("input").val("");
      Fancybox.show([{ src: "#thanks-sign-up", type: "inline" }]);
      $(".appointment").trigger("reset");
    });
    return false;
  });
});

// /**
//  * @license
//  * Copyright 2019 Google LLC. All Rights Reserved.
//  * SPDX-License-Identifier: Apache-2.0
//  */
// function initMap() {
//   const myLatlng = { lat: 34.16393025, lng: -118.2803277247166 };
//   const map = new google.maps.Map(document.getElementById("map"), {
//     zoom: 17,
//     center: myLatlng,
//   });
//   const marker = new google.maps.Marker({
//     position: myLatlng,
//     map,
//     title: "ELITE DENTAL GROUP, 1249 W. Glenoaks Blvd Glendale, CA 91201",
//     icon: "../img/marker.png",
//   });
// }

// window.initMap = initMap;
